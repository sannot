#include <jack/jack.h>

#ifndef SANNOT_H
#define SANNOT_H

struct annotation {
  jack_nframes_t frame;
  int indentation;
  char* text;

  struct annotation* next;
  struct annotation* prev;
};

enum command
{
  none,
  start,
  stop,
  toggle_startstop,
  transport_forward,
  transport_fforward,
  transport_rewind,
  transport_frewind,
  home,
  input,
  edit,
  indent,
  unindent,
  up,
  down,
  quit
};
#endif
