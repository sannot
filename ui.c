#include <stdio.h>
#include <curses.h>
#include "sannot.h"

extern struct annotation * annotations;
extern struct annotation * current_annotation;
extern struct annotation * selected_annotation;

void init_ui()
{
  WINDOW * screen = initscr();
  raw();
  noecho();
  cbreak();
  //nodelay(screen, TRUE);
  halfdelay(10);
}

void refresh_ui(jack_nframes_t frame)
{
  clear();
  struct annotation * annotation = annotations; 
  int i = 1;
  mvprintw(0, 0, "%d", frame);  
  while (annotation != NULL)
  {
    if (annotation == current_annotation)
    {
      mvprintw(i, 0, "*");  
    }
    if (annotation == selected_annotation)
    {
      mvprintw(i, 1, ">");  
    }
    mvprintw(i, 2 + annotation->indentation * 3, "%d - %s", annotation->frame, annotation->text);  

    annotation = annotation->next;
    i++;
  }
}

void exit_ui()
{
  clear();
  endwin();
}
