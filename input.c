#include <stdio.h>
#include "input.h"

enum command getCommand()
{
  char character = getch();
  switch(character)
  {
    case 'q':
      return quit;
    case ' ':
      return toggle_startstop;
    case 10:
      return input;
    case 'i':
      return edit;
    case 65:
      return up;
    case 66:
      return down;
    case 67:
      return transport_forward;
    case 68:
      return transport_rewind;
    case 72:
      return home;
    case 9:
      return indent;
    case 90:
      return unindent;
    default:
      fprintf(stderr, "%d\n", character);
  }
  return none;
}
