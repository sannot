#include <jack/jack.h>

/**
 * Query the jack transport to find the current frame
 */
jack_nframes_t transport_refresh(jack_client_t* client);

void transport_toggle(jack_client_t* client);

void transport_start(jack_client_t* client);

void transport_stop(jack_client_t* client);

void transport_move(jack_client_t* client, int delta);

void transport_moveto(jack_client_t* client, jack_nframes_t location);
