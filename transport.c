#include "transport.h"

/**
 * Query the jack transport to find the current frame
 */
jack_nframes_t transport_refresh(jack_client_t* client)
{
  jack_position_t current;
  jack_transport_query(client, &current);
  return current.frame;
}

void transport_toggle(jack_client_t* client)
{
  jack_position_t current;
  if (jack_transport_query(client, &current) != JackTransportStopped)
  {
    transport_stop(client);
  }
  else
  { 
    transport_start(client);
  }
}

void transport_stop(jack_client_t* client)
{
  jack_transport_stop(client);
}

void transport_start(jack_client_t* client)
{
  jack_transport_start(client);
}

void transport_move(jack_client_t* client, int delta)
{
  jack_nframes_t location = transport_refresh(client);
  if (delta < 1 && ((0-delta) > location))
    location = 0;
  else
    location += delta;
  jack_transport_locate(client, location);
}

void transport_moveto(jack_client_t* client, jack_nframes_t location)
{
  jack_transport_locate(client, location);
}
