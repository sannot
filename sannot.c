#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <jack/jack.h>
#include "sannot.h"
#include "ui.h"
#include "input.h"

struct annotation * annotations = NULL;

struct annotation * current_annotation = NULL;

struct annotation * selected_annotation = NULL;

// find the last annotation before this frame (if any)
struct annotation * annotationAt(jack_nframes_t frame)
{
  struct annotation * current = annotations;
  struct annotation * previous = NULL; 
  while (current != NULL)
  {
    if (current->frame > frame)
    {
      return previous;
    }
    previous = current;
    current = current->next;
  }
  return previous;
}

void addannotation(struct annotation * annotation)
{
  struct annotation * previous = annotationAt(annotation->frame);
  if (previous == NULL)
  {
    annotation->next = annotations;
    annotations = annotation;
  }
  else
  {
    annotation->prev = previous;
    if (previous->next != NULL)
    {
      previous->next->prev = annotation;
    }
    previous->next = annotation;
  }
  selected_annotation = annotation;
}

void createannotation(jack_nframes_t frame, int indentation, char* text)
{
  struct annotation * new_annotation = (struct annotation*) malloc(sizeof(struct annotation));
  new_annotation->frame = frame;
  new_annotation->indentation = 0;
  new_annotation->text = text;
  new_annotation->next = NULL;
  new_annotation->prev = NULL;
  addannotation(new_annotation);
}

int size = 80;
void editannotation(jack_client_t * client)
{
  if (selected_annotation == NULL)
    return;
  mvprintw(0,0,"Enter text for this annotation: ");
  echo();
  getnstr(selected_annotation->text, size);
  noecho();
  halfdelay(10);
}

void newannotation(jack_client_t * client)
{
  jack_nframes_t frame = transport_refresh(client);
  char* text = (char*) malloc(size * sizeof(char));
  mvprintw(0,0,"Enter text for new annotation: ");
  echo();
  getnstr(text, size);
  noecho();
  halfdelay(10);
  
  createannotation(frame, 0, text);
}

int main()
{
  bool quitRequested = false;
  jack_client_t * client = jack_client_open("sannot", JackNullOption, NULL);
  if (client == NULL)
  {
    fprintf(stderr, "could not create JACK client\n");
    return 1;
  }
  
  // Main loop: when a command is entered or when the jack transport reaches 
  // a new annotation, handle this and refresh the screen.
  init_ui();
  refresh_ui(transport_refresh(client));
  while (!quitRequested)
  {
    struct annotation * new_annotation = annotationAt(transport_refresh(client));
    refresh_ui(transport_refresh(client));
    if (new_annotation != current_annotation)
    {
      current_annotation = new_annotation;
      refresh_ui(transport_refresh(client));
    }
    // waits for the next command - times out if none arrives, so 
    // we can check if we need to view another annotation. At some point we
    // might want to implement this as a 'real' multi-threaded event loop, but
    // for now this should do fine.
    switch (getCommand())
    {
      case none:
        break;
      case quit:
        quitRequested = true;
        break;
      case start:
        transport_start(client);
        break;
      case stop:
        transport_stop(client);
        break;
      case toggle_startstop:
        transport_toggle(client);
        break;
      case transport_rewind:
        transport_move(client, -40000);
        break;
      case transport_forward:
        transport_move(client, 40000);
        break;
      case home:
        transport_moveto(client, 0);
        break;
      case edit:
        editannotation(client);
        refresh_ui(transport_refresh(client));
        break;
      case input:
        newannotation(client);
        refresh_ui(transport_refresh(client));
        break;
      case up:
        if (selected_annotation != NULL && selected_annotation->prev != NULL)
          selected_annotation = selected_annotation->prev;
        break;
      case down:
        if (selected_annotation != NULL && selected_annotation->next != NULL)
          selected_annotation = selected_annotation->next;
        break;
      case indent:
        if (selected_annotation != NULL)
          selected_annotation->indentation++;
        break;
      case unindent:
        if (selected_annotation != NULL && selected_annotation->indentation > 0)
          selected_annotation->indentation--;
        break;
      //refresh_ui();
    }
  }

  exit_ui();
  jack_client_close(client);
  
  return 0;
}
